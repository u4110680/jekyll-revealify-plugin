require 'pathname'

module Jekyll
  class JekyllRevealify < Generator
    priority :high
    safe true

    def generate(site)
      root = Pathname.new(Bundler.rubygems.find_name('jekyll-revealify-plugin').first.full_gem_path)

      #
      # 1. Get static reveal.js assets into Jekyll
      #
      folders = ["reveal.js", "reveal.js-plugins", "chalkboard-redux"]
      folders.each do |folder|
        files = Dir[File.join(root.to_s, "/#{folder}/**/*.*")]

        files = files.map do |f|
          abs = Pathname.new(File.expand_path(f))

          abs.relative_path_from(root.join("#{folder}")).to_s
        end

        files = files.map do |f|
          StaticFile.new(site, root.to_s, "#{folder}", f)
        end

        site.static_files.concat(files)
      end
    end
  end
end
